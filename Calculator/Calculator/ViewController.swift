//
//  ViewController.swift
//  Calculator
//
//  Created by Tim Yencken on 15/06/2016.
//  Copyright © 2016 Stanford University Example. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // number - display
    @IBOutlet private weak var display: UILabel!
    
    // flag - user - typing
    private var userIsInTheMiddleOfTyping = false
    
    // function - touch - digit
    @IBAction private func touchDigit(sender: UIButton) {
        let digit = sender.currentTitle!
        if userIsInTheMiddleOfTyping {
            let textCurentlyInDisplay = display.text!
            display.text = textCurentlyInDisplay + digit
        } else {
            display.text = digit
        }
        userIsInTheMiddleOfTyping = true
    }
    
    // calculated - display - value
    private var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            display.text = String(newValue)
        }
    }

    // init - model - brain
    private var brain = CalculatorBrain()
    
    // function - perform - operation
    @IBAction private func performOperation(sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(mathematicalSymbol)
        }
        displayValue = brain.result
    }
    
}

